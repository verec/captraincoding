//
//  BePatient.swift
//  CapTrainCoding
//
//  Created by verec on 14/07/2016.
//  Copyright © 2016 Cantabilabs Ltd. All rights reserved.
//

import Foundation
import UIKit

class BePatient : UIView {

    var backColor : UIColor = UIColor(white: 0.9, alpha: 1.0) {
        didSet {
            self.setNeedsDisplay()
        }
    }

    var foreColor : UIColor = UIColor(white: 0.705, alpha: 1.0) {
        didSet {
            self.setNeedsDisplay()
        }
    }

    var value: Math.UnitValue = 0.0 {
        didSet {
            self.setNeedsDisplay()
        }
    }

    var enabled:Bool = true {
        didSet {
            if oldValue != enabled {
                UIView.animateWithDuration(0.3) {
                    self.alpha = self.enabled ? 1.0 : 0.0
                }
            }
        }
    }

    convenience init() {
        self.init(frame:CGRect.zero)
    }

    override init(frame:CGRect) {
        super.init(frame:frame)

        self.backgroundColor = UIColor.clearColor()
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

extension BePatient {

    override func drawRect(rect: CGRect) {

        if self.bounds.isEmpty {
            return
        }

        guard let context = UIGraphicsGetCurrentContext() else {
            return
        }

        let bounds = self.bounds.insetBy(dx: 5.0, dy: 0.0)
        let min = bounds.minX
        let mid = min + value * bounds.width
        let max = min + bounds.width

        let backPoints: [CGPoint] = [
            CGPoint(x: min, y: bounds.midY - 0.5)
        ,   CGPoint(x: max, y: bounds.midY - 0.5)
        ]
        
        let forePoints: [CGPoint] = [
            CGPoint(x: min, y: bounds.midY - 0.5)
        ,   CGPoint(x: mid, y: bounds.midY - 0.5)
        ]

        CGContextSetLineWidth(context, 3.0)
        CGContextSetLineCap(context, .Round)
        CGContextSetStrokeColorWithColor(context, backColor.CGColor)
        CGContextStrokeLineSegments(context, backPoints, 2)
        CGContextSetStrokeColorWithColor(context, foreColor.CGColor)
        CGContextStrokeLineSegments(context, forePoints, 2)
    }
}