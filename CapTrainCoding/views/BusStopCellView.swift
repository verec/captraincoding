//
//  BusStopCellView.swift
//  CapTrainCoding
//
//  Created by verec on 14/07/2016.
//  Copyright © 2016 Cantabilabs Ltd. All rights reserved.
//

import Foundation
import UIKit

import QuartzCore

class BusStopCellView: UIView {

    static let iso8601Formmater:NSDateFormatter = {
        let dateFormatter = NSDateFormatter()
        let enUSPosixLocale = NSLocale(localeIdentifier: "en_US_POSIX")
        dateFormatter.locale = enUSPosixLocale
        dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ssZZZZZ"
        return dateFormatter
    } ()

    static let defaultFormatter:NSDateFormatter = {
        let dateFormatter = NSDateFormatter()
        let locale = NSLocale.currentLocale()
        dateFormatter.locale = locale
        dateFormatter.dateFormat = "HH:mm"
        return dateFormatter
    } ()


    let logo    =   UIImageView()
    let plch    =   UIActivityIndicatorView(activityIndicatorStyle: .Gray)

    let name    =   UILabel()
    let desc    =   UILabel()
    let time    =   UILabel()

    var cell: BusStopCellModel.Cell? {
        didSet {
            applyCell()
            self.setNeedsLayout()
        }
    }

    convenience init() {
        self.init(frame:CGRect.zero)
    }

    override init(frame:CGRect) {
        super.init(frame:frame)

        setup()

        self.setNeedsLayout()
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

extension BusStopCellView {

    func setup() {
        self.addSubview(logo)
        self.addSubview(plch)

        self.addSubview(name)
        self.addSubview(desc)
        self.addSubview(time)

        setupLabel(name, font: Fonts.MainPage.nameCellFont, color: UIColor(white: 0.4, alpha: 1.0))
        setupLabel(desc, font: Fonts.MainPage.descCellFont, color: UIColor(white: 0.3, alpha: 1.0))
        setupLabel(time, font: Fonts.MainPage.timeCellFont, color: UIColor(white: 0.5, alpha: 1.0))
    }

    func setupLabel(label: UILabel, text: String? = .None, font: UIFont? = .None, color: UIColor? = .None) {

        if let text = text      {   label.text = text       }
        if let font = font      {   label.font = font       }
        if let color = color    {   label.textColor = color }

        label.backgroundColor = UIColor.clearColor()
        label.textAlignment = .Left
    }
}

extension BusStopCellView {

    func applyNewTime(newTime:String) {
        let oldTime = time.text
        let animate = oldTime == .None || oldTime! != newTime

        if animate {
            UIView.animateWithDuration(1.3, animations: {

                self.time.textColor = UIColor.redColor()

                }, completion: { (completed) in

                    UIView.animateWithDuration(1.3) {
                        self.time.textColor = UIColor(white: 0.5, alpha: 1.0)
                    }
            })
        }
        time.text = newTime
    }

    func applyCell() {
        guard let cell = self.cell else {
            return
        }

        if let stop_name = cell.stop.stop_name {
            name.text = stop_name
        } else {
            name.text = .None
        }

        if let stop_desc = cell.stop.stop_desc {
            desc.text = stop_desc
        } else {
            desc.text = .None
        }

        if let dep_time = cell.departureTime {
            if let date = BusStopCellView.iso8601Formmater.dateFromString(dep_time) {
                let asString = BusStopCellView.defaultFormatter.stringFromDate(date)
                applyNewTime(asString)
            } else {
                time.text = .None
            }
        } else {
            time.text = .None
        }

        if cell.main {
            self.backgroundColor = UIColor(white: 0.90, alpha: 1.0)
        } else {
            self.backgroundColor = UIColor.clearColor()
        }

        logo.image = UIImage(named: "bus")

        logo.bounds = CGRect.zero.square(40.0)

        self.setNeedsLayout()
        self.setNeedsDisplay()
    }
}

extension BusStopCellView {

    override func layoutSubviews() {
        super.layoutSubviews()

        if !self.bounds.isDefined {
            return
        }

        let logoView:UIView
        if let _ = logo.image {
            plch.stopAnimating()
            plch.hidden = true
            logo.hidden = false
            logoView = logo
        } else {
            plch.hidden = false
            logo.hidden = true
            plch.startAnimating()
            logoView = plch
        }

        logoView.frame = CGRect.zero.square(40.0).centered(intoRect: self.bounds)
            .edge(.Left, alignedToRect: self.bounds)
            .edge(.Left, offsetBy: -10.0)

        name.sizeToFit()
        desc.sizeToFit()
        time.sizeToFit()

        let columns = self.bounds.tabStops([0.25, 0.71])
        name.frame = name.bounds.positionned(intoRect: self.bounds, widthUnitRange: 0.5, heightUnitRange: 0.33)
            .edge(.Left, alignedToRect: columns[0])
        time.frame = time.bounds.positionned(intoRect: self.bounds, widthUnitRange: 0.5, heightUnitRange: 0.50)
            .edge(.Left, alignedToRect: columns[1])
        desc.frame = desc.bounds.positionned(intoRect: self.bounds, widthUnitRange: 0.5, heightUnitRange: 0.66)
            .edge(.Left, alignedToRect: columns[0])
    }
}


