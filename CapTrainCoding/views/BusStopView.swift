//
//  BusStopView.swift
//  CapTrainCoding
//
//  Created by verec on 14/07/2016.
//  Copyright © 2016 Cantabilabs Ltd. All rights reserved.
//

import Foundation


import UIKit

class BusStopView : UIView {

    var busStopCellTable: UITableView?

    private struct Parameters {
        static let cellClass                = BusStopCell.self
        static let cellIdentifier           = NSStringFromClass(cellClass)
    }

    lazy var maxRows:Int = {
//        let cm = ScreenSizeModel.currentModel
//        if cm == ScreenSizeModel.screenSize_3_5in {
//            return 5
//        } else if cm == ScreenSizeModel.screenSize_4_0in {
//            return 6
//        } else if cm == ScreenSizeModel.screenSize_4_7in {
//            return 7
//        }
//        return 8
        return 9
    }()

    /// public
    convenience init() {
        self.init(frame:CGRect.zero)
    }

    override init(frame:CGRect) {
        super.init(frame:frame)

        self.busStopCellTable = UITableView(frame: self.bounds, style: .Plain)
        self.busStopCellTable?.delegate = self
        self.busStopCellTable?.dataSource = self
        self.busStopCellTable?.registerClass(Parameters.cellClass, forCellReuseIdentifier: Parameters.cellIdentifier)

        self.busStopCellTable?.showsHorizontalScrollIndicator = false
        self.busStopCellTable?.showsVerticalScrollIndicator = true
        self.busStopCellTable?.separatorStyle = .None

        self.busStopCellTable?.backgroundColor = UIColor.clearColor()
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

extension BusStopView {

    override func layoutSubviews() {

        super.layoutSubviews()

        guard let busStopCellTable = self.busStopCellTable else {
            return
        }

        if busStopCellTable.orphaned {
            self.addSubview(busStopCellTable)

            self.addSubview(Views.Main.bePatient)
        }

        let insetX = CGFloat(30.0)
        let insetY = CGFloat(60.0)

        let top = self.bounds.top(insetY)
        let mid = self.bounds.insetBy(dx: insetX, dy: insetY)
//        let bot = self.bounds.bottom(insetY)

        Views.Main.bePatient.frame = top
        busStopCellTable.frame = mid
    }
}

extension BusStopView {

    func reloadContents() {
        self.busStopCellTable?.reloadSections(NSIndexSet(index:0), withRowAnimation: .None)
    }

    func scrollToMainRow() {
        for (index, cell) in WellKnown.Modelling.busCellsModel.cells.enumerate() {
            if cell.main {
                let ip = NSIndexPath(forRow: index, inSection: 0)
                self.busStopCellTable?.scrollToRowAtIndexPath(ip, atScrollPosition: .Middle, animated: true)
            }
        }
    }

    func reload(index: Int) {
        if let visibleRows = self.busStopCellTable?.indexPathsForVisibleRows {
            let ips = visibleRows.filter { $0.row == index }
            /// make sure there's at least one row before refreshing the first
            if let ip = ips.first {
                self.busStopCellTable?.reloadRowsAtIndexPaths([ip], withRowAnimation: .None)
            }
        }
    }
}

extension BusStopView {

    func bind(cellValue: BusStopCellModel.Cell?, cell: BusStopCell) {
        cell.view.cell = cellValue
    }
}


extension BusStopView : UITableViewDelegate {

    /// technically a UITableViewDelegate _is a_ UIScrollViewDelegate, but it
    /// clearer to separate what belongs to the scrolView from what belongs
    /// to the tableView.

    func fixedRowHeight() -> CGFloat {
        if let busStopCellTable = busStopCellTable {
            return busStopCellTable.bounds.height / CGFloat(maxRows)
        }
        return 0.0
    }

    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        return fixedRowHeight()
    }
}

extension BusStopView : UIScrollViewDelegate {

    /// technically a UITableViewDelegate _is a_ UIScrollViewDelegate, but it
    /// clearer to separate what belongs to the scrolView from what belongs
    /// to the tableView.

    /// This is so that when the table free scrolls in an intertial move it
    /// stops at cell boundaries, never displaying an incomplete cell.

    func scrollViewWillEndDragging(
        scrollView:                 UIScrollView
        ,   withVelocity velocity:      CGPoint
        ,   targetContentOffset:        UnsafeMutablePointer<CGPoint>) {

        targetContentOffset.memory = scrollView.snapToIntegerRowForProposedContentOffset(
            targetContentOffset.memory
        ,   rowHeight:  self.fixedRowHeight())
    }
}

extension BusStopView : UITableViewDataSource {

    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }

    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return WellKnown.Modelling.busCellsModel.count
    }

    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {

        guard let cell = tableView.dequeueReusableCellWithIdentifier(Parameters.cellIdentifier) as? BusStopCell else {
            assert(false, "This just cannot happen")
            return UITableViewCell()
        }

        if let cellValue = WellKnown.Modelling.busCellsModel[indexPath.row] {
            bind(cellValue, cell: cell)
        }

        return cell
    }
}