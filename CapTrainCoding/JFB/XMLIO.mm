//
//  XMLIO.mm
//  HBSVG
//
//  Created by verec on 14/04/2013.
//  Copyright (c) 2013 Inspired Now Ltd. All rights reserved.
//
#import <CoreGraphics/CGBase.h>
#import <CoreGraphics/CGGeometry.h>

//#import "DDLog.h"
//DD_LOG_LEVEL(LOG_LEVEL_WARN) ;

#import "XMLIO.h"
#import "SExp.h"
#import "SExpIO.h"

@interface XMLIO ()

@property (nonatomic, strong)   NSMutableArray *        stack ;

@end

@implementation XMLIO

//+ (BOOL) resolveClassMethod: (SEL) name {
//    BOOL sr = [super resolveClassMethod:name] ;
//    NSString * s = ::NSStringFromSelector(name) ;
//    DDLogInfo(@"[%@-%08x] resolving (+) %@ %@", [self class], (int) self, s, sr ? @"succeeded" : @"*** FAILED ***") ;
//    return sr ;
//}
//
//+ (BOOL) resolveInstanceMethod: (SEL) name {
//    BOOL sr = [super resolveInstanceMethod:name] ;
//    NSString * s = ::NSStringFromSelector(name) ;
//    DDLogInfo(@"[%@-%08x] resolving (-) %@ %@", [self class], (int) self, s, sr ? @"succeeded" : @"*** FAILED ***") ;
//    return sr ;
//}

+ (XMLIO *) xmlIo {
    return [[XMLIO alloc] init] ;
}

+ (int) tabCount {
    return 2 ;
}

+ (NSData *) encode: (SExp *) root {
    NSMutableString * s = [NSMutableString string] ;
    NSString * r = [self write:root toString:s] ;
    return [r dataUsingEncoding:NSUTF8StringEncoding] ;
}

+ (NSString *) write: (SExp *) root toString: (NSMutableString *) s {
    return [self write: root toString: s atLevel: 0] ;
}

+ (NSString *) format: (NSString *) text isNumber: (out BOOL *) isNumber {
    NSDictionary * props = [SExpIO properties] ;
    NSString * fmt = nil ;
    if (props && [props count] && (fmt = props[SEXPIO_EXPORT_FLOATING_POINT_FORMAT])) {
        int count = (int) [text length] ;
        for (int i = 0 ; i < count ; ++i) {
            unichar c = [text characterAtIndex:i] ;
            if (c == '.' || c == '-' || ((c >= '0') && (c <= '9'))) continue ;
            return text ;
        }
        // special case for when text is exactly "."
        if (count == 1 && [text characterAtIndex:0] == '.') {
            return text ;
        }
        // special case for when text is exactly "-"
        if (count == 1 && [text characterAtIndex:0] == '-') {
            return text ;
        }
        *isNumber = YES ;
        CGFloat threshold = [props[SEXPIO_EXPORT_INTEGER_FLOAT_THRESHOLD] floatValue] ;
        CGFloat value = [text floatValue] ;
        NSInteger intValue = (NSInteger) value ;
        CGFloat error = value - (CGFloat) intValue ;
        if (error < threshold) {
            return [NSString stringWithFormat:@"%ld", (long)intValue] ;
        }
        return [NSString stringWithFormat:fmt, value] ;
    }
    return text ;
}

+ (NSString *) write: (SExp *) root toString: (NSMutableString *) outString atLevel: (NSInteger) level {

    if (level) {
        [outString appendFormat: @"\n%*s<%@", (int) ([self tabCount] * level), " ", root.token] ;
    } else {
        [outString appendFormat: @"\n<%@", root.token] ;
    }
    BOOL hasChildren = [root.children count] > 0 ;
    
    if ([root.attributes count] > 0) {
        for (NSString * attr in root.attributes) {
            BOOL isNumber = NO ;
            NSString * attrStr = [self format:[attr description] isNumber:&isNumber] ;
            NSString * closer = hasChildren ? @"" : @"/" ;
            if (isNumber) {
                [outString appendFormat:@" value=%@%@>", attrStr, closer] ;
            } else {
                [outString appendFormat:@" value=\"%@\"%@>", attrStr, closer] ;
            }
        }
    } else {
        [outString appendString: @">"] ;

    }

    for (SExp * child in root.children) {
        [self write: child toString: outString atLevel: 1+level] ;
    }
//
//    if (level) {
//        [outString appendFormat: @"\n%*s</%@>", [self tabCount] * level, " ", root.token] ;
//    } else {
//        [outString appendFormat: @"\n</%@>", root.token] ;
//    }

    if (hasChildren) {
        [outString appendFormat: @"</%@>", root.token] ;
    }

    return outString ;
}


+ (SExp *) decode: (NSData *) data {
    XMLIO * xmlIo = [XMLIO xmlIo] ;

    SExp * root  = [SExp sexp: @"<root>"] ;
    xmlIo.stack = [NSMutableArray arrayWithCapacity:20] ;
    [xmlIo.stack addObject:root] ;
    
    NSXMLParser *parser = [[NSXMLParser alloc] initWithData:data] ;

    parser.delegate = xmlIo ;

    [parser parse] ;
    
    return root ;
}

#pragma mark NSXMLParserDelegate

- (void) parserDidStartDocument: (NSXMLParser *) parser {

}

- (void) parserDidEndDocument: (NSXMLParser *) parser {

}

- (void) parser: (NSXMLParser *) parser
didStartElement: (NSString *) elementName
   namespaceURI: (NSString *) namespaceURI
  qualifiedName: (NSString *) qName
     attributes: (NSDictionary *) dict {

    SExp * curr = [self.stack lastObject] ;
    SExp * child = [SExp sexp: elementName] ;
    [curr.children addObject:child] ;
    
    [self.stack addObject:child] ;

    for (NSString * key in dict) {
        NSString * value = dict[key] ;
        SExp * attr = [SExp sexp:key] ;
        [attr addAttribute:value] ;
        [child.children addObject:attr] ;
    }
}

- (void) parser: (NSXMLParser *) parser
  didEndElement: (NSString *) elementName
   namespaceURI: (NSString *) namespaceURI
  qualifiedName: (NSString *) qName {

    [self.stack removeLastObject] ;
}

- (void) parser: (NSXMLParser *) parser
foundCharacters: (NSString *) string {

    NSCharacterSet * ws = [NSCharacterSet whitespaceAndNewlineCharacterSet] ;
    string = [string stringByTrimmingCharactersInSet:ws] ;
    
    if ([string length] == 0) {
        return ;
    }
    
    SExp * curr = [self.stack lastObject] ;
    if ([curr.attributes count] == 0) {
        [curr.attributes addObject:string] ;
    } else {
        NSString * s = curr.attributes[0] ;
        s = [s stringByAppendingString:string] ;
        curr.attributes[0] = s ;
    }
}

- (void) parser: (NSXMLParser *) parser
     foundCDATA: (NSData *) CDATABlock {

    NSString * s = [[NSString alloc] initWithData:CDATABlock encoding:NSUTF8StringEncoding] ;
    SExp * curr = [self.stack lastObject] ;
    SExp * cdata= [SExp sexp: @"[CDATA]"];
    [cdata addAttribute:s] ;
    [curr.children addObject:cdata] ;
}

- (void) parser: (NSXMLParser *) parser
parseErrorOccurred: (NSError *) parseError {
    NSLog(@"*** parseErrorOccurred:%@", parseError) ;
}

- (void) parser: (NSXMLParser *) parser
validationErrorOccurred: (NSError *) validationError {
    NSLog(@"*** validationErrorOccurred:%@", validationError) ;
}


@end
