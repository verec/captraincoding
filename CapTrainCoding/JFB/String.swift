//
//  String.swift
//  __core_sources
//
//  Created by verec on 31/10/2015.
//  Copyright © 2015 Cantabilabs Ltd. All rights reserved.
//

import Foundation

extension String {

    var NS: NSString {
        return self as NSString
    }
}

extension String {

    func trim() -> String {
        return self.stringByTrimmingCharactersInSet(NSCharacterSet.whitespaceAndNewlineCharacterSet())
    }

    func trimQuote() -> String {
        return self.stringByTrimmingCharactersInSet(NSCharacterSet(charactersInString: "\""))
    }

    func split(delimiter:String) -> [String] {
        let s = self as NSString
        let a = s.componentsSeparatedByString(delimiter)
        return a as [String]
    }

    static func join(strings: [String], withSeparator sep: String = "") -> String {
        let s = (strings as NSArray).componentsJoinedByString(sep)
        return s as String
    }
}


