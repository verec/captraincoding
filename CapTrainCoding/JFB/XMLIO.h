//
//  XMLIO.h
//  HBSVG
//
//  Created by verec on 14/04/2013.
//  Copyright (c) 2013 Inspired Now Ltd. All rights reserved.
//

#import <Foundation/Foundation.h>

@class SExp ;

@interface XMLIO : NSObject<NSXMLParserDelegate>

+ (NSData *) encode: (SExp   *) sexp ;
+ (SExp   *) decode: (NSData *) data ;

@end
