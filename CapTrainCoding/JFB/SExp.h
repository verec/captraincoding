//
//  SExp.h
//  __core_sources
//
//  Created by verec on 26/06/2010.
//  Copyright 2010 Jean-François Brouillet. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreGraphics/CGGeometry.h>

//@import Foundation ;
//@import CoreGraphics.CGGeometry ;

//#import <CoreGraphics/CGGeometry.h>

@interface SExp : NSObject <NSCopying>

@property (nonatomic, strong) NSMutableArray<SExp * > * _Nonnull        children ;
@property (nonatomic, strong) NSMutableArray<NSString *> * _Nonnull     attributes ;
@property (nonatomic, strong) NSString *	_Nullable	token ;

// returns a new SExp with no token and no children
+ (instancetype _Nonnull) sexp ;

// returns a new SExp with specified token but no children
+ (instancetype _Nonnull) sexp: (NSString * _Nonnull) token ;

// returns a new new SExp with no token and no children, creating first a root
// SExp whose path to the returned SExp node is specifed by path. Uses the
// customary "sub1/sub2/ .../subn" path format specifier

+ (instancetype _Nullable) sexp: (NSString * _Nonnull) path root: (SExp * _Nonnull * _Nonnull) root ;

- (NSString * _Nonnull) description ;

- (instancetype _Nullable) objectForKeyedSubscript: (id <NSCopying> _Nonnull) key ;


@end

@interface SExp (Attributes)

- (SExp * _Nonnull) add: (SExp * _Nonnull) child ;

- (SExp * _Nonnull) addAttribute: (NSString * _Nonnull) attribute ;
- (SExp *_Nonnull) setAttribute: (NSString *_Nonnull ) attribute index: (NSInteger) index ;

- (BOOL) attribute: (NSString * _Nullable * _Nonnull) value ;
- (BOOL) attribute: (NSString * _Nullable * _Nonnull) value at: (NSUInteger) index ;

- (NSString * _Nullable) firstAttribute ;

@end

@interface SExp (Creation)

- (SExp * _Nonnull) ensurenode: (NSString * _Nonnull) path ;

@end

@interface SExp (Navigation)

- (BOOL) matches: (NSString * _Nonnull) target ;

// if path can point to at most one node returns it, or nil if no such node.
- (SExp * _Nonnull) subnode: (NSString * _Nonnull) path ;

// if path can point to more than one node at the final level, then sibling
// is the index of the desired node, or nil if no such node exists.
- (SExp * _Nonnull) subnode: (NSString * _Nonnull) path
           sibling: (NSUInteger) sibling ;
@end

@interface SExp (Iteration)
// Iterates over all children, calling `block'
- (void) each: (void (^ _Nonnull)(SExp * _Nonnull child)) block ;

// Iterates over all children, path away from self, calling `block', `path'
// away from self. Uses the customary "sub1/sub2/ .../subn" path format specifier
- (void) each: (NSString * _Nonnull) path block:(void (^ _Nonnull)(SExp * _Nonnull)) block ;

@end

@interface SExp (Deprecated)

- (BOOL) stringValue: (NSString * _Nullable * _Nonnull) value ;
- (BOOL) stringValue: (NSString * _Nullable * _Nonnull) value at: (NSUInteger) index ;

- (SExp * _Nonnull) addIntAttribute:     (int)           attribute ;
- (SExp * _Nonnull) addFloatAttribute:   (float)         attribute ;
- (SExp * _Nonnull) addBoolAttribute:    (BOOL)          attribute ;
- (SExp * _Nonnull) addCGPointAttribute: (CGPoint)       attribute ;

- (SExp * _Nonnull) addDateAttribute:    (NSDate * _Nonnull)      attribute ;
- (BOOL) dateValue: (NSDate * _Nullable * _Nonnull) value ;
- (BOOL) dateValue: (NSDate * _Nullable * _Nonnull) value at: (NSUInteger) index ;

- (BOOL) unicharValue: (unichar * _Nonnull) value ;
- (BOOL) unicharValue: (unichar * _Nonnull) value at: (NSUInteger) index ;
- (BOOL) intValue: (int * _Nonnull) value ;
- (BOOL) intValue: (int * _Nonnull) value at: (NSUInteger) index ;
- (BOOL) CGFloatValue: (CGFloat * _Nonnull) value ;
- (BOOL) CGFloatValue: (CGFloat * _Nonnull) value at: (NSUInteger) index ;
- (BOOL) boolValue: (BOOL * _Nonnull) value ;
- (BOOL) boolValue: (BOOL * _Nonnull) value at: (NSUInteger) index ;

- (BOOL) CGPointValue: (CGPoint * _Nonnull) value ;
- (BOOL) CGPointValue: (CGPoint * _Nonnull) value at: (NSUInteger) index ;

@end