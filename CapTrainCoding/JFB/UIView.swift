//
//  UIView.swift
//  __core_sources
//
//  Created by verec on 05/09/2015.
//  Copyright © 2015 Cantabilabs Ltd. All rights reserved.
//

import Swift

import Foundation
import UIKit

extension UIView {

    enum HomeButtonLocation : String {

        case    Top
        case    Left
        case    Bottom
        case    Right
        case    Other

        init(deviceOrientation: UIDeviceOrientation) {
            switch deviceOrientation {
                case    .Unknown:               self = Other
                case    .Portrait:              self = Bottom
                case    .PortraitUpsideDown:    self = Top
                case    .LandscapeLeft:         self = Right
                case    .LandscapeRight:        self = Left
                case    .FaceUp:                self = Other
                case    .FaceDown:              self = Other
            }
        }
    }
}

extension UIView {

    func dismissKeyboard() {

        func doDismiss(fromView: UIView) {
            fromView.endEditing(true)
        }

        for window in UIApplication.sharedApplication().windows {
            doDismiss(window)
        }
    }
}

extension UIView {

    func forceLayout() {
        self.setNeedsLayout()
        self.layoutIfNeeded()
    }

    func deepSetNeedsLayout() {
        self.setNeedsLayout()
        for view in self.subviews {
            view.deepSetNeedsLayout()
        }
    }

    func forceTintColorAllTheWayDownStartingWithView(view: UIView, tintColor: UIColor) {
        view.tintColor = tintColor
        for v in view.subviews {
            if v is UIImageView {
                let imageView = v as! UIImageView
                if let image = imageView.image where imageView.tag != 1357111317 {
                    imageView.image = image.imageWithRenderingMode(.AlwaysTemplate)
                }
            }
            forceTintColorAllTheWayDownStartingWithView(v, tintColor:tintColor)
        }
    }

    func digFirstImageView(view: UIView) -> UIImageView? {
        if view is UIImageView {
            return (view as! UIImageView)
        }

        for v in view.subviews {
            if let imageView = digFirstImageView(v) {
                return imageView
            }
        }

        return .None
    }
}

extension UIView {
    var origin:CGPoint {
        var o = self.frame.topLeft
        if !(self is UIWindow) {
            if let s = self.superview {
                let k = s.origin
                o.x += k.x
                o.y += k.y
            }
        }
        return o
    }
}

extension UIView {

    var orphaned:Bool {
        return self.superview == nil
    }

    func snpashot() -> UIImage {
        let scale = UIScreen.mainScreen().scale
        UIGraphicsBeginImageContextWithOptions(self.bounds.size, true, scale)
        self.drawViewHierarchyInRect(self.bounds, afterScreenUpdates: true)
        let image = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        return image
    }

    typealias PostOverlay = (CGContextRef) -> ()

    func layerSnpashot(postEffect:Bool = true, customEffect:PostOverlay? = .None) -> UIImage {
        /// FIXME: use scale rather than 0 -> 6+/6S+ ... ???
        let scale = UIScreen.mainScreen().scale

        UIGraphicsBeginImageContextWithOptions(self.bounds.size, false, scale)

        if let context = UIGraphicsGetCurrentContext() {
            if postEffect {
                self.layer.renderInContext(context)
                if  let customEffect = customEffect {
                    customEffect(context)
                }
            } else {
                if  let customEffect = customEffect {
                    customEffect(context)
                }
                self.layer.renderInContext(context)
            }
        }

        let image = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        
        return image
    }
}
