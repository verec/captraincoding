//
//  BusScheduleView.swift
//  CapTrainCoding
//
//  Created by verec on 13/07/2016.
//  Copyright © 2016 Cantabilabs Ltd. All rights reserved.
//

import Foundation
import UIKit

class BusScheduleView : UIView {

    convenience init() {
        self.init(frame:CGRect.zero)
    }

    override init(frame:CGRect) {
        super.init(frame:frame)
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

extension BusScheduleView {

    override func layoutSubviews() {
        super.layoutSubviews()

        if Views.Main.busStopsView.orphaned {
            self.addSubview(Views.Main.busStopsView)
        }

        Views.Main.busStopsView.frame = self.bounds
    }
}