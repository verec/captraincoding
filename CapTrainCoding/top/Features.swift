//
//  Features.swift
//  CapTrainCoding
//
//  Created by verec on 14/07/2016.
//  Copyright © 2016 Cantabilabs Ltd. All rights reserved.
//

import Foundation

struct Features {
    static let reducedLocationPrecision     =   true

    struct Tooling {
        static let buildStopTimeSExp        =   false
    }

    struct Runtime {
        static let useStopSequenceSExp      =   !Tooling.buildStopTimeSExp
    }
}