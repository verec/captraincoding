//
//  AppDelegate.swift
//  CapTrainCoding
//
//  Created by verec on 13/07/2016.
//  Copyright © 2016 Cantabilabs Ltd. All rights reserved.
//

import QuartzCore

import Foundation
import UIKit

/// A partir des données opendata de Rennes Métropole (http://data.keolis-rennes.com/fr/les-donnees/donnees-et-api.html),
/// afficher les prochains bus passant aux arrêts à proximité.

/// The general strategy for this exercise:
/// "display the bus stop & next bus service closest to where I stand"
/// 1. know where I am => use core location + fake test location in simulator
/// 2. load static data about bus routes, stops & time
/// 3. load dynamic data about XYZ
/// 4. setup UI with no known location to start with
/// 5. when there is a location change: work out
/// 5a. which of the bus stops is closest to said location
/// 5b. which times the next buses are expected at, at that loaction
/// 5c. update the UI accordingly.

/// - look into "stops" for a bunch (say, at mnost 3?) stops closest to me
/// - for each such stop, fetch its trip_id
/// - sort trip_id according to sequence (stop order. hopefully time order?)
/// - discard any trip whose calendar says service not running today, or time out of range
/// - merge all trips in starting with the first "closest" one result is a
///   lnear array
/// - each element of the array is itself an array with
/// - trip id/trip name, direction, time to next bus

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?

    var initialized = false {
        didSet {
            startLocationQueries()
        }
    }

    func setupWindow() {

        self.window = Top.mainWindow {
            Top.defaultController.orientation = [
                .Portrait,.PortraitUpsideDown
            ]

            Top.defaultController.statusBarStyle = .Default /// .LightContent
            Top.mainView = Views.Main.topView
        }

        self.window?.backgroundColor = UIColor.whiteColor()
    }

    func localLoad() {
        /// Serialqueue#1 is for the network IO, SerialQueue#2 is for local
        /// processing
        let start = CACurrentMediaTime()
        GCD.SerialQueue2.async {
            WellKnown.Modelling.starNetwork.load {
                assert(NSThread.isMainThread())
                /// now populate the UI/setup the model by enabling locations
                print("trips locally loaded \(WellKnown.Modelling.starNetwork.trips.count) in \(CACurrentMediaTime() - start)s")
                print("stops loaded \(WellKnown.Modelling.starNetwork.stops.count) in \(CACurrentMediaTime() - start)s")
                self.initialized = true
            }
        }
    }

    func startLocationQueries() {
        let status = WellKnown.Policies.location.runnableStatus()
        if status == .NeedToAsk {
            WellKnown.Policies.location.ask()
        } else if status == .Granted {
            WellKnown.Policies.location.updated = {
                WellKnown.Policies.logic.applyLocation($0)
            }
        }
    }
}

extension AppDelegate {

    func application(application: UIApplication, didFinishLaunchingWithOptions launchOptions: [NSObject: AnyObject]?) -> Bool {

        setupWindow()
        localLoad()

        WellKnown.Policies.timedProgress.timeLimit = Values.progressTimeOut + CACurrentMediaTime()
        return true
    }
}

extension AppDelegate {

    func applicationWillResignActive(application: UIApplication) {
        // Sent when the application is about to move from active to inactive
        // state. This can occur for certain types of temporary interruptions
        // (such as an incoming phone call or SMS message) or when the user
        // quits the application and it begins the transition to the background
        // state. Use this method to pause ongoing tasks, disable timers, and
        // throttle down OpenGL ES frame rates. Games should use this method to
        // pause the game.
    }

    func applicationDidEnterBackground(application: UIApplication) {
        // Use this method to release shared resources, save user data, 
        // invalidate timers, and store enough application state information to
        // restore your application to its current state in case it is
        // terminated later. If your application supports background execution,
        // this method is called instead of applicationWillTerminate: when the 
        // user quits.
    }

    func applicationWillEnterForeground(application: UIApplication) {
        // Called as part of the transition from the background to the inactive
        // state; here you can undo many of the changes made on entering the
        // background.
    }

    func applicationDidBecomeActive(application: UIApplication) {

        // Restart any tasks that were paused (or not yet started) while the
        // application was inactive. If the application was previously in the 
        // background, optionally refresh the user interface.

        if initialized {
            GCD.MainQueue.async {
                self.startLocationQueries()
            }
        }
    }

    func applicationWillTerminate(application: UIApplication) {
        // Called when the application is about to terminate. Save data if
        // appropriate. See also applicationDidEnterBackground:.
    }
}

