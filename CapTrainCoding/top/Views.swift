//
//  Views.swift
//  CapTrainCoding
//
//  Created by verec on 13/07/2016.
//  Copyright © 2016 Cantabilabs Ltd. All rights reserved.
//

import Foundation
import UIKit

struct Views {

    struct Main {
        static let topView          =   BusScheduleView()
        static let busStopsView     =   BusStopView()
        static let bePatient        =   BePatient()
    }
}