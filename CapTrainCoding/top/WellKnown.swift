//
//  WellKnown.swift
//  CapTrainCoding
//
//  Created by verec on 13/07/2016.
//  Copyright © 2016 Cantabilabs Ltd. All rights reserved.
//

import Foundation

struct WellKnown {

    struct Network {
        static let basicRestGET     = BasicRestGET()
        static let captainConf      = CaptainConf()
        static let captainLoader    = CaptainLoad()
        static let jsonParser       = JSONParser()
    }

    struct Modelling {
        static var starNetwork      = StarNetwork()
        static var stations         = Model.Stations()
        static var departures       = Model.Departures()
        static var busCellsModel    = BusStopCellModel()
    }

    struct Policies {
        static let location         = Location()
        static var logic            = Logic()
        static let refresher        = Refresher()
        static let timedProgress    = TimedProgress()
    }
}