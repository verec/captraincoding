//
//  Values.swift
//  CapTrainCoding
//
//  Created by verec on 14/07/2016.
//  Copyright © 2016 Cantabilabs Ltd. All rights reserved.
//

import Foundation

struct Values {

    struct Location {
        static let tooFarFromUserDistance:Double    = 500.0 /// meters
    }

    static let progressTimeOut:NSTimeInterval       = 6.0
}