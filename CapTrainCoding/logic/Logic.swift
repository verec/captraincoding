//
//  Logic.swift
//  CapTrainCoding
//
//  Created by verec on 14/07/2016.
//  Copyright © 2016 Cantabilabs Ltd. All rights reserved.
//
import QuartzCore

import Foundation
import CoreLocation

func == (lhs: CLLocationCoordinate2D, rhs: CLLocationCoordinate2D) -> Bool {

    if Features.reducedLocationPrecision {

        let lhsLat = Int(lhs.latitude * 1000.0)
        let lhsLon = Int(lhs.longitude * 1000.0)
        let rhsLat = Int(rhs.latitude * 1000.0)
        let rhsLon = Int(rhs.longitude * 1000.0)

        return lhsLat == rhsLat && lhsLon == rhsLon
    }

    return lhs.latitude == rhs.latitude && lhs.longitude == rhs.longitude
}

struct Logic {

    var lastLocation:CLLocation? = .None

    mutating func applyLocation(currentLocation: CLLocation) {
        if let lastLocation = lastLocation where lastLocation.coordinate == currentLocation.coordinate {
            return
        }
        self.lastLocation = currentLocation
        applyLocationInner(currentLocation)
    }

    /// from stop                       =>  stop_id
    /// from stop_times[stop_id]        =>  trip_id
    /// from trips[trip_id]             =>  service_id, route_id
    /// from calendar[service_id]       =>  start date, end date, monday-friday (sat/sun)
    /// from calendar_dates[service_id] =>  date + exception type
    /// from routes[route_id]           => description

    func applyLocationInner(currentLocation: CLLocation) {
        print("location now: \(currentLocation)")

        /// this primes the refresher on the first ever update
        let _ = WellKnown.Policies.refresher

        /// use serial queue#2 for computations
        GCD.SerialQueue2.async {

            /// sort the stops array by distance to newLocation
            let newStops = WellKnown.Modelling.starNetwork.stops.sort {
                let first = $0
                let scnd  = $1

                if  let lat1 = first.stop_lat
                ,   let lon1 = first.stop_lon
                ,   let lat2 = scnd.stop_lat
                ,   let lon2 = scnd.stop_lon {

                    let loc1 = CLLocation(latitude: lat1, longitude: lon1)
                    let loc2 = CLLocation(latitude: lat2, longitude: lon2)

                    return loc1.distanceFromLocation(currentLocation) <= loc2.distanceFromLocation(currentLocation)
                }

                return false

                /// and filter out stops that are way too far (currently: 500m)
            }.filter {
                if  let lat1 = $0.stop_lat
                ,   let lon1 = $0.stop_lon {
                    let loc1 = CLLocation(latitude: lat1, longitude: lon1)
                    return loc1.distanceFromLocation(currentLocation) <= Values.Location.tooFarFromUserDistance
                }
                return false
            }

            if let closestStop = newStops.first {

                if  let mainStopID  = closestStop.stop_id
                ,   let trips       = WellKnown.Modelling.starNetwork.tripsIDByStopID[mainStopID]
                ,   let tripID      = trips.first
                ,   let stops       = WellKnown.Modelling.starNetwork.stopsByTripID[tripID] {

                    let cells:[BusStopCellModel.Cell] = stops.map {
                        let stopid = $0
                        let cellStop: StarNetwork.Stop
                        let main:Bool
                        if let stop = WellKnown.Modelling.starNetwork.stopsByID[stopid] {
                            cellStop = stop
                            main = stopid == mainStopID
                        } else {
                            cellStop = StarNetwork.Stop(values:[""])
                            main = false
                        }

                        return BusStopCellModel.Cell(stop:cellStop, departureTime: .None, main: main)
                    }

                    /// perform an intermediate refresh of the UI before
                    /// fetching the time tables
                    GCD.MainQueue.async {
                        WellKnown.Modelling.busCellsModel.cells = cells
                    }

                    self.refreshTimes()
                }


            } else {
                GCD.MainQueue.async {
                    WellKnown.Modelling.busCellsModel.cells = []
                }
            }
        }
    }

    func refreshTimes() {
        GCD.SerialQueue2.async {
            var cells:[BusStopCellModel.Cell] = []
            /// always always switch to the main thread before accessing the
            /// model, by performing a _sync_ (not async) call.
            GCD.MainQueue.sync {
                cells = WellKnown.Modelling.busCellsModel.cells
            }

            /// FIXME:
            /// We should batch the requests by 10 as per the spec
            /// but I'd need to write a generic `collect` on SequenceType
            /// and I just do not have the time, so ... the good old way!

            for (index, var cell) in cells.enumerate() {

                guard let stopid = cell.stop.stop_id else {
                    continue
                }

                WellKnown.Network.captainLoader.load([
                    "version":  Command.Version2_1.version
                ,   "cmd":      Command.Version2_1.getbusnextdepartures]
                ,   params:     [
                        ["mode":"stop"]
                    ,   ["stop][":"\(stopid)"]
                    ,   ["direction":"1"]]) {

                    (data) in

                    assert(NSThread.isMainThread())

                    if let data = data, let jsonDict = WellKnown.Network.captainLoader.json(data) {
                        if let departures: Model.Departures = WellKnown.Network.jsonParser.decode(jsonDict) {
                            if let dep = departures.departures?.first {
                                cell.departureTime = dep.departure
                                WellKnown.Modelling.busCellsModel[index] = cell
                                print("refreshing stop id: \(stopid) with departure: \(dep.departure)")
                            }
                        }
                    }
                }
            }
        }
    }
}

class Refresher : NSObject {

    var refreshTimer: NSTimer! = nil

    override init() {
        super.init()

        self.refreshTimer = NSTimer.scheduledTimerWithTimeInterval(
                                59.0
            ,   target:         self
            ,   selector:       #selector(Refresher.tick(_:))
            ,   userInfo:       nil
            ,   repeats:        true)

    }

    func tick(timer: NSTimer) {
        if let _ = WellKnown.Policies.logic.lastLocation {
            WellKnown.Policies.logic.refreshTimes()
        }
    }
}