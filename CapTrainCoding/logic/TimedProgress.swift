//
//  TimedProgress.swift
//  CapTrainCoding
//
//  Created by verec on 14/07/2016.
//  Copyright © 2016 Cantabilabs Ltd. All rights reserved.
//

import QuartzCore

import Foundation
import UIKit

class TimedProgress : NSObject {

    private
    var refreshTimer: NSTimer! = nil

    private
    var timeStart: NSTimeInterval?

    var timeLimit: NSTimeInterval? {
        didSet {
            if let _ = self.timeLimit {
                timeStart = CACurrentMediaTime()
                if !enabled {
                    enabled = true
                }
            }
        }
    }

    var enabled: Bool {
        get {
            return self.refreshTimer != nil
        }
        set {
            if self.refreshTimer != nil {
                self.refreshTimer!.invalidate()
                self.refreshTimer = nil
            }

            if newValue {
                Views.Main.bePatient.value = 0.0
                Views.Main.bePatient.enabled = true
                self.refreshTimer = NSTimer.scheduledTimerWithTimeInterval(
                                        0.050
                    ,   target:         self
                    ,   selector:       #selector(TimedProgress.tick(_:))
                    ,   userInfo:       nil
                    ,   repeats:        true)
            } else {
                Views.Main.bePatient.enabled = false
            }
        }
    }
}

extension TimedProgress {

    func tick(timer: NSTimer) {
        let now = CACurrentMediaTime()
        guard let timeLimit = timeLimit else {
            return
        }
        guard let timeStart = timeStart else {
            return
        }
        let unit = timeLimit - timeStart
        let elapsed = now - timeStart
        let value = elapsed / unit

        if value > 1.0 {
            self.enabled = false
        } else {
            Views.Main.bePatient.value = Math.UnitValue(Math.clip(value, min:0.0, max:1.0))
        }
    }
}
