//
//  WellKnown.swift
//  CapTrainCoding
//
//  Created by verec on 13/07/2016.
//  Copyright © 2016 Cantabilabs Ltd. All rights reserved.
//

import Foundation
import CoreLocation

class Location : NSObject {

    var updated: ((CLLocation)->())?

    enum RunnableState {
        case    Denied
        case    NeedToAsk
        case    Granted
    }

    func runnableStatus() -> RunnableState {
        let status = CLLocationManager.authorizationStatus()
        if status == .NotDetermined {
            return .NeedToAsk
        }

        if status == .Restricted || status == .Denied {
            return .Denied
        }
        return .Granted
    }

    let clObj:CLLocationManager

    override init() {
        clObj = CLLocationManager()
        super.init()
        clObj.activityType = .Fitness
        clObj.delegate = self

        clObj.startUpdatingLocation()
    }

    func ask() {
        clObj.requestWhenInUseAuthorization()
    }
}

extension Location : CLLocationManagerDelegate {

    func locationManager(manager: CLLocationManager, didChangeAuthorizationStatus status: CLAuthorizationStatus) {
        print(status)

        if status == .AuthorizedWhenInUse {
            print("starting location monitoring")
            manager.startUpdatingLocation()
        } else  if status == .Denied {
            print("denied")
        }
    }

    func locationManager(manager: CLLocationManager, didFailWithError error: NSError) {
        print("locationManager:didFailWithError: \(error)")
    }

    func locationManager(manager: CLLocationManager, didFinishDeferredUpdatesWithError error: NSError?) {
        print("locationManager:didFinishDeferredUpdatesWithError: \(error)")
    }

    func locationManager(manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {

        if let location = locations.last {
            self.updated?(location)
        }
    }
}