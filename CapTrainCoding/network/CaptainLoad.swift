//
//  CaptainLoad.swift
//  CapTrainCoding
//
//  Created by verec on 13/07/2016.
//  Copyright © 2016 Cantabilabs Ltd. All rights reserved.
//

import Foundation

struct CaptainLoad {

    typealias ParseCompletion = (NSData?)->()

    func json(data: NSData?) -> [String:AnyObject]? {
        /// JSON decoding is fast enough
        if let data = data {
            do {
                if let parsed = try NSJSONSerialization.JSONObjectWithData(data, options: [.MutableContainers, .AllowFragments]) as? [String:AnyObject] {
                    return parsed
                }
            } catch {
                print(error)
            }
        }
        return .None
    }

    func load(commands: [String:String], params:[[String:String]]? = .None, uiCompletion: ParseCompletion) {

        func callCompletion(rawData: NSData? = .None) {
            GCD.MainQueue.async {
                uiCompletion(rawData)
            }
        }

        if let url = WellKnown.Network.captainConf.captainURL(commands, params: params) {

            WellKnown.Network.basicRestGET.load(fullURL: url) {
                (error, data) in

                assert(NSThread.isMainThread())

                if let _ = data {
                    callCompletion(data)
                    return
                } else if let error = error {
                    print(error)
                }

                callCompletion()
            }
        } else {
            callCompletion()
        }
    }
}