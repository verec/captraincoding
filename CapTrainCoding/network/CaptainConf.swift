//
//  CaptainConf.swift
//  CapTrainCoding
//
//  Created by verec on 13/07/2016.
//  Copyright © 2016 Cantabilabs Ltd. All rights reserved.
//

import Foundation

struct CaptainConf {

    /*
 
     Note: this being iOS9 and the endpoint being http (not https) this requires
     a special entry in the plist:

     <key>NSAppTransportSecurity</key>
     <dict>
        <key>NSAllowsArbitraryLoads</key>
            <true/>
     </dict>

     */

    let apiKey   = "E7Y2BTC5HJDNX17"
    let endPoint = "http://data.keolis-rennes.com/json/"

    func configuredEndPoint(query:[String:String]? = .None, params:[[String:String]]? = .None) -> String {

        let baseDict:[String:String] = [
            "key":      apiKey
        ]

        var configured = endPoint
        var sep = "?"

        func appendQueryItemsFromDict(dict: [String:String]) {
            for (key, value) in dict {
                configured += "\(sep)\(key)=\(value)"
                sep = "&"
            }
        }

        appendQueryItemsFromDict(baseDict)
        if let query = query {
            appendQueryItemsFromDict(query)
        }
        if let params = params {
            for param in params {
                for (key, value) in param {
                    let paramKey = "param[\(key)]"
                    appendQueryItemsFromDict([paramKey:value])
                }
            }
        }

        return configured
    }

    func captainURL(query:[String:String]? = .None, params:[[String:String]]? = .None) -> NSURL? {
        return NSURL(string:self.configuredEndPoint(query, params: params))
    }
}