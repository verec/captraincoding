//
//  Commands.swift
//  CapTrainCoding
//
//  Created by verec on 13/07/2016.
//  Copyright © 2016 Cantabilabs Ltd. All rights reserved.
//

import Foundation

struct Command {
    struct Version1_0 {
        static let version                  =   "1.0"
        static let getdistrict              =   "getdistrict"
        static let getstation               =   "getstation"
    }

    struct Version2_0 {
        static let version                  =   "2.0"
    }

    struct Version2_1 {
        static let version                  =   "2.1"
        static let getbusnextdepartures     =   "getbusnextdepartures"
    }

    struct Version2_2 {
        static let version                  =   "2.2"
        static let getbusnextdepartures     =   "getbusnextdepartures"
    }
}