//
//  StarNetwork.swift
//  CapTrainCoding
//
//  Created by verec on 13/07/2016.
//  Copyright © 2016 Cantabilabs Ltd. All rights reserved.
//

import QuartzCore

import Foundation
import CoreLocation

struct StarNetwork {

    typealias Loaded = ()->()

    struct Trip {
        let route_id                    :   String?
        let service_id                  :   String?
        let trip_id                     :   String?
        let trip_headsign               :   String?
        let trip_short_name             :   String?
        let direction_id                :   String?
        let block_id                    :   String?
        let shape_id                    :   String?
        let wheelchair_accessible       :   String?
        let bikes_allowed               :   String?

        init(values:[String]) {
            route_id                = values.count > 0 ? values[0] : .None
            service_id              = values.count > 1 ? values[1] : .None
            trip_id                 = values.count > 2 ? values[2] : .None
            trip_headsign           = values.count > 3 ? values[3] : .None
            trip_short_name         = values.count > 4 ? values[4] : .None
            direction_id            = values.count > 5 ? values[5] : .None
            block_id                = values.count > 6 ? values[6] : .None
            shape_id                = values.count > 7 ? values[6] : .None
            wheelchair_accessible   = values.count > 8 ? values[8] : .None
            bikes_allowed           = values.count > 9 ? values[9] : .None
        }
    }

    struct Stop {
        let stop_id                     :   Int?
        let stop_code                   :   String?
        let stop_name                   :   String?
        let stop_desc                   :   String?
        let stop_lat                    :   CLLocationDegrees?
        let stop_lon                    :   CLLocationDegrees?
        let zone_id                     :   String?
        let stop_url                    :   String?
        let location_type               :   String?
        let parent_station              :   String?
        let stop_timezone               :   String?
        let wheelchair_boarding         :   String?

        init(values:[String]) {
            stop_id                 = values.count > 0 ? values[0].NS.integerValue : .None
            stop_code               = values.count > 1 ? values[1] : .None
            stop_name               = values.count > 2 ? values[2] : .None
            stop_desc               = values.count > 3 ? values[3] : .None
            stop_lat                = values.count > 4 ? values[4].NS.doubleValue : .None
            stop_lon                = values.count > 5 ? values[5].NS.doubleValue : .None
            zone_id                 = values.count > 6 ? values[6] : .None
            stop_url                = values.count > 7 ? values[6] : .None
            location_type           = values.count > 8 ? values[8] : .None
            parent_station          = values.count > 9 ? values[9] : .None
            stop_timezone           = values.count > 10 ? values[10] : .None
            wheelchair_boarding     = values.count > 11 ? values[11] : .None
        }
    }

    struct StopTime {
        let trip_id                     :   Int?
        let arrival_time                :   String?
        let departure_time              :   String?
        let stop_id                     :   Int?
        let stop_sequence               :   Int?
        let stop_headsign               :   String?
        let pickup_type                 :   String?
        let drop_off_type               :   String?
        let shape_dist_traveled         :   String?

        init(values:[String]) {
            trip_id                 = values.count > 0 ? values[0].NS.integerValue : .None
            arrival_time            = values.count > 1 ? values[1] : .None
            departure_time          = values.count > 2 ? values[2] : .None
            stop_id                 = values.count > 3 ? values[3].NS.integerValue : .None
            stop_sequence           = values.count > 4 ? values[4].NS.integerValue : .None
            stop_headsign           = values.count > 5 ? values[5] : .None
            pickup_type             = values.count > 6 ? values[6] : .None
            drop_off_type           = values.count > 7 ? values[7] : .None
            shape_dist_traveled     = values.count > 8 ? values[8] : .None
        }
    }

    var trips = [Trip]()

    mutating func addTrip(values:[String]) {
        self.trips.append(Trip(values:values))
    }
    
    var stops = [Stop]()
    var defaultStop = Stop(values: [""])

    mutating func addStop(values:[String]) {
        self.stops.append(Stop(values:values))
    }

    var stopTimes = [StopTime]()

    mutating func addStopTimes(values:[String]) {
        self.stopTimes.append(StopTime(values:values))
    }

    var stopsByTripID   = [Int:[Int]]()
    var tripsIDByStopID = [Int:[Int]]()
    var stopsByID       = [Int:Stop]()

    typealias Slurp = ([String])->()

    static let bundlePrefix = "static/GTFS-20160706"

    ///
    /// replaces 540440 lines such as: (yes. that's half a million!!)
    ///
    /// trip_id,arrival_time,departure_time,stop_id,stop_sequence,stop_headsign,pickup_type,drop_off_type,shape_dist_traveled
    /// "1","14:05:00","14:05:00","1270","4","","0","0",""
    /// "1","14:03:00","14:03:00","1449","2","","0","0",""
    /// ...
    /// "9999","14:45:00","14:45:00","3024","12","","0","0",""
    ///
    /// with 111433 lines such as:
    ///
    ///  (stop-sequence
    ///    (trip 1
    ///      (stops
    ///       1534 1449 1450 1270 2262 2704 2907 2918 2919 2920 2921 2922 2923 2924 2927
    ///       2930 3909 3910 3911 3912 3904 3914 3915 3901 4701 4703 4705 4707 4708))
    ///
    ///    (trip 2
    ///      (stops
    ///       1534 1449 1450 1270 2262 2704 2907 2918 2919 2920 2921 2922 2923 2924 2927
    ///       2930 3909 3910 3911 3912 3904 3914 3915 3901 4701 4703 4705 4707 4708))
    ///
    ///    ...
    ///    (trip 23215
    ///      (stops
    ///       5030 5029 5028 5027 5026 5025 5024 5023 5022 5021 5020 5019 5018 5017 5001))
    ///
    ///    (trip 23216
    ///      (stops
    ///       5030 5029 5028 5027 5026 5025 5024 5023 5022 5021 5020 5019 5018 5017 5001)))
    ///
    /// reducing the file size from 29.8MB down to 3.5MB
    ///
    /// The parsing time of the SExp is about 6 seconds as opposed to the 30 seconds
    /// of the half a million line file ...

    func buildTimeConvertHugeCSVFileToSExp() {
        let start = CACurrentMediaTime()
        /// first stort by trip id
        let stopTimesSortedByTripID = stopTimes.sort {

            let st0 = $0
            let st1 = $1

            guard let tr0   = st0.trip_id else          { return false }
            guard let tr1   = st1.trip_id else          { return false }
            guard let seq0  = st0.stop_sequence else    { return false }
            guard let seq1  = st1.stop_sequence else    { return false }

            if tr0 == tr1 {
                return seq0 < seq1
            }

            return tr0 < tr1
        }

        /// stops are now sorted by trip-id first, and sequence second

        let root = SExp.sexp("stop-sequence")
        var lastTripId:Int? = .None
        var currentStops = SExp.sexp()
        for stopTime in stopTimesSortedByTripID {

            guard let tripID = stopTime.trip_id else { continue }
            guard let stopid = stopTime.stop_id else { continue }

            if lastTripId == .None || lastTripId! != tripID {
                lastTripId = tripID
                let currentTrip = SExp.sexp("trip").addAttribute("\(tripID)")
                root.add(currentTrip)
                currentStops = SExp.sexp("stops")
                currentTrip.add(currentStops)
            }

            currentStops.addAttribute("\(stopid)")
        }

        let path = "~/Documents/stop-sequence.sexp".NS.stringByExpandingTildeInPath
        let data = SExpIO.encode(root)
        data.writeToFile(path, atomically: true)

        print("buildTimeConvertHugeCSVFileToSExp in \(CACurrentMediaTime() - start)")
    }

    // FIXME: those files probably mean something, we need to parse them too
    // in real life ...
    //
    //      ("agency", { (f) in })
    //    ,	("calendar", { (f) in })
    //    ,	("calendar_dates", { (f) in })
    //    ,	("feed_info", { (f) in })
    //    ,	("routes", { (f) in })
    //    ,	("routes_additionals", { (f) in })
    //    ,	("stops_extensions", { (f) in })

    mutating func load(loaded: Loaded? = .None) {

        let start = CACurrentMediaTime()
        var files:[(String, Slurp)] = [
            ("stops", { self.addStop($0) })
        ,   ("trips", { self.addTrip($0) })
        ]

        if !Features.Runtime.useStopSequenceSExp {
            files.append(("stop_times", { self.addStopTimes($0) }))
        }

        for (file, slurp) in files {
            if let path = NSBundle.mainBundle().pathForResource(file, ofType: "txt", inDirectory: StarNetwork.bundlePrefix) {
                do {
                    let contents = try NSString(contentsOfFile: path, encoding: NSUTF8StringEncoding)
                    let lines = contents.componentsSeparatedByCharactersInSet(NSCharacterSet.newlineCharacterSet())
                    for (index, line) in lines.enumerate() {
                        if index == 0 {
                            /// skip the header
                            continue
                        }
                        let fields = line.componentsSeparatedByString(",").map { $0.trimQuote()}
                        slurp(fields)
                    }
                } catch {
                    print("error: \(error)")
                }
            }
        }

        print("slurping static in \(CACurrentMediaTime() - start)s")

        for stop in stops {
            if let stopid = stop.stop_id {
                stopsByID[stopid] = stop
            }
        }

        if Features.Runtime.useStopSequenceSExp {

            let start = CACurrentMediaTime()

            if let path = NSBundle.mainBundle().pathForResource("stop-sequence", ofType: "sexp", inDirectory: "static/processed") {
                if let data = NSData(contentsOfFile: path) {
                    if  let root = SExpIO.decode(data)
                    ,   let seq = root.children[0] as? SExp {

                        seq.each {
                            let trip = $0
                            if let tripIDs = trip.firstAttribute() {
                                let tripID = tripIDs.NS.integerValue
                                if let stopExp = trip.children[0] as? SExp {
                                    let stops:[Int] = stopExp.attributes.map {
                                        $0.integerValue
                                    }
                                    self.stopsByTripID[tripID] = stops

                                    for stopID in stops {
                                        if var trips = self.tripsIDByStopID[stopID] {
                                            trips.append(tripID)
                                            self.tripsIDByStopID[stopID] = trips
                                        } else {
                                            self.tripsIDByStopID[stopID] = [tripID]
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
            print("parsing seqeunce stop SExp file in \(CACurrentMediaTime() - start)s")

        } else {

            if Features.Tooling.buildStopTimeSExp {
                buildTimeConvertHugeCSVFileToSExp()
            }

            for stopTime in stopTimes {
                guard let stopID = stopTime.stop_id else { continue }

                if let tripID = stopTime.trip_id {
                    if var stops = stopsByTripID[tripID] {
                        stops.append(stopID)
                        stopsByTripID[tripID] = stops
                    } else {
                        stopsByTripID[tripID] = [stopID]
                    }

                    if var trips = tripsIDByStopID[stopID] {
                        trips.append(tripID)
                        tripsIDByStopID[stopID] = trips
                    } else {
                        tripsIDByStopID[stopID] = [tripID]
                    }
                }
            }
        }

        GCD.MainQueue.async {
            loaded?()
        }
    }
}