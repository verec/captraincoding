//
//  JSONParser.swift
//  CapTrainCoding
//
//  Created by verec on 13/07/2016.
//  Copyright © 2016 Cantabilabs Ltd. All rights reserved.
//

import Foundation

struct JSONParser {

    func extract(key:String, container:[String:AnyObject]) -> [String:AnyObject]? {
        return container[key] as? [String:AnyObject]
    }

    func extractArray(key:String, container:[String:AnyObject]) -> [[String:AnyObject]]? {
        return container[key] as? [[String:AnyObject]]
    }

    func decodeData(jsonDict: [String:AnyObject]) -> [String:AnyObject]? {
        guard let opendata = extract("opendata", container: jsonDict) else {
            return .None
        }

        guard let answer = extract("answer", container: opendata) else {
            return .None
        }

        guard let status = extract("status", container: answer) else  {
            return .None
        }

        if let attr = extract("@attributes", container: status) {
            if let code = attr["code"] as? String where "\(code)" != "0" {
                return .None
            }
        }

        guard let data = extract("data", container: answer) else  {
            return .None
        }

        return data
    }
}

extension JSONParser {

    func decode(jsonDict: [String:AnyObject]) -> Model.Stations? {
        guard let data = decodeData(jsonDict) else {
            return .None
        }

        guard let stationsJS = extractArray("station", container: data) else  {
            return .None
        }

        var stations = Array<Model.Station>()

        for stationJS in stationsJS {
            let id              = stationJS["id"] ?? .None
            let number          = stationJS["number"] ?? .None
            let name            = stationJS["name"] ?? .None
            let state           = stationJS["state"] ?? .None
            let latitude        = stationJS["latitude"] ?? .None
            let longitude       = stationJS["longitude"] ?? .None
            let slotsavailable  = stationJS["slotsavailable"] ?? .None
            let bikesavailable  = stationJS["bikesavailable"] ?? .None
            let pos             = stationJS["pos"] ?? .None
            let district        = stationJS["district"] ?? .None
            let lastupdate      = stationJS["lastupdate"] ?? .None

            let station = Model.Station(
                id:             "\(id)"
            ,   number:         "\(number)"
            ,   name:           "\(name)"
            ,   state:          "\(state)"
            ,   latitude:       "\(latitude)"
            ,   longitude:      "\(longitude)"
            ,   slotsavailable: "\(slotsavailable)"
            ,   bikesavailable: "\(bikesavailable)"
            ,   pos:            "\(pos)"
            ,   district:       "\(district)"
            ,   lastupdate:     "\(lastupdate)")

            stations.append(station)
        }
        
        return Model.Stations(stations: stations)
    }
}

extension JSONParser {

    func decode(jsonDict: [String:AnyObject]) -> Model.Departures? {
        guard let data = decodeData(jsonDict) else {
            return .None
        }

        guard let attr = extract("@attributes", container: data) else  {
            return .None
        }

        var departures = Array<Model.Departure>()
        if let time = attr["localdatetime"] as? String {
            departures.append(Model.Departure(departure: time))
        }

        return Model.Departures(departures: departures)
    }
}