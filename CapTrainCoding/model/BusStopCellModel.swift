//
//  BusStopCellModel.swift
//  CapTrainCoding
//
//  Created by verec on 14/07/2016.
//  Copyright © 2016 Cantabilabs Ltd. All rights reserved.
//

import Foundation

struct BusStopCellModel {

    /// this is what gets blasted into a UITableViewCell
    struct Cell {
        let stop: StarNetwork.Stop
        var departureTime: String? = .None
        let main: Bool
    }

    var cells = [Cell]() {
        didSet {
            Views.Main.busStopsView.reloadContents()
            Views.Main.busStopsView.scrollToMainRow()
        }
    }

    var count:Int {
        return cells.count
    }

    subscript(index:Int) -> Cell? {

        get {
            if count > index {
                return cells[index]
            }
            return .None
        }

        set (newValue) {
            if let newValue = newValue where count > index {
                cells[index] = newValue
                Views.Main.busStopsView.reload(index)
            }
        }
    }
}