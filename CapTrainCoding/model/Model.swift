//
//  Model.swift
//  CapTrainCoding
//
//  Created by verec on 13/07/2016.
//  Copyright © 2016 Cantabilabs Ltd. All rights reserved.
//

import Foundation

struct Model {

    struct Station {
        let id :                String?
        let number :            String?
        let name :              String?
        let state :             String?
        let latitude :          String?
        let longitude :         String?
        let slotsavailable :    String?
        let bikesavailable :    String?
        let pos  :              String?
        let district  :         String?
        let lastupdate :        String?
    }

    struct Departure {
        let departure :         String?
    }

    struct Stations {
        var stations : [Station]?
    }

    struct Departures {
        var departures : [Departure]?
    }
}

func == (lhs: Model.Departure, rhs: Model.Departure) -> Bool {
    if  let lhdep = lhs.departure
    ,   let rhdep = rhs.departure {

        return lhdep == rhdep
    }
    return false
}

func == (lhs: Model.Departures, rhs: Model.Departures) -> Bool {

    if  let lhdep = lhs.departures
    ,   let rhdep = rhs.departures {

        if lhdep.count != rhdep.count {

            return false
        }

        for (index, left) in lhdep.enumerate() {

            let right = rhdep[index]
            if !(right == left) {

                return false
            }
        }

        return true
    }

    return false
}