# CapTrainCoding Tests  

This is a coding test using Xcode 7.3.1 & Swift 2.2.  
A number of issues, presented below, are still present, but I spent way (way!) more than 4 hours on this, and I had to draw the line somewhere.  

### Update Sunday 17 July  
I was just too annoyed about the 30s boot time and deciced to do something about it. I now convert the ~ `500,000` lines, ~ `30 MB` _stop-times.txt_ into a ~ `11,000` lines ~ `3.5MB` `.sexp`  file, resulting in:   
   
- 6s for initial load as opposed to 30s  
- 70MB or RAM at the peak as opposed to 1.2GB  

This conversion is done only once with the Simulator and manually copied into the bundle resources, such that:  

      "1","14:05:00","14:05:00","1270","4","","0","0",""   
      "1","14:03:00","14:03:00","1449","2","","0","0",""   

is replaced with:  

     (stop-sequence  
       (trip 1  
         (stops  
           1534 1449 1450 1270 2262 2704 2907 2918 2919 2920 2921 2922 2923 2924 2927  
           2930 3909 3910 3911 3912 3904 3914 3915 3901 4701 4703 4705 4707 4708))  
         
       (trip 2  
         (stops  
           1534 1449 1450 1270 2262 2704 2907 2918 2919 2920 2921 2922 2923 2924 2927
           2930 3909 3910 3911 3912 3904 3914 3915 3901 4701 4703 4705 4707 4708))
              
           ...

The conversion is turned on or off by the _Feature_ flag `buildStopTimeSExp`:  

    struct Features {  
        ...
        struct Tooling {
            static let buildStopTimeSExp        =   true
        }
        ...
    }

See [Wikipedia](https://en.wikipedia.org/wiki/S-expression) for details of the `.sexp`  file format.    

## Status  
- the app runs  
- you need to specify a geo location close to one of the stops next to Rennes in the simulator, eg: `lat=48.119241` & `lon=-1.667693` in order to see anything  
- the app filters out stops that are more than 500m away from the above location  
- the departure time seems always to reflect the _current time_ maybe because this is a demo API or my API key triggers demo data?  
- though the time is displayed in the user time zone, even though transmitted in iso8601  
- I have no idea that the stop I am displaying data for is indeed a Bus Stop. It could be a metro station for all know. I am just assuming.
- the first ever updates results in noticeable flicker, sorry :(

## Hurdles before starting  
- the app needs an API key for which a (free) registration is required  
- the documentation is a bit lacking  
- confusion about the purpose & differences between the static data model, that which has to be downloaded from Google (eg the files such as `trips.txt`, `stops.txt`, `stop_times.txt`, etc ...) and what kind of dynamic data exists that needs runtime fetching via `REST`/`JSON`  

## Hurdles during development  
- the "static" files `stop_times.txt` and `trips.txt`are huge (29MB and 1.6MB respectively) and composed of over 500,000 lines (yes that is half a million) and use up to 1.2GB of RAM for pocessing, as well as eating up to 30s for decoding! This makes the app far from production ready!  
- to mitigate this problem, I display a progress bar for 30s on startup on an otherwise blank screen
- I have tried to split the file in smaller KB size chunks, with the intention of reading them _on the fly_ but ended up with over 23,000 files for `trips.txt` once split, leading to build times of more than 7 minutes (transferring that many files to the device/simulator doesn't seem to please Xcode that much), so I gave up. This problem would need to be addressed for a real app.  _(see `derived_trips` and `derived_stops` directories)_  

## Conceptual Issues I brushed aside  
- the difference between route, trip and service is subtle at times.  
- normally, I would expect a given stop to be serviced by at least one service/trip, but in practice I ended up getting loads of services (more than 10!) for every single stops. Either I misunderstood something (very, very likely) or there's some issue with the static data  
- so I ended up only considering the 1st trip associated with the closest stop, and used that trip to list the other stops from the same trip in order to display a "route" somehow reconstructed thusly.
- in real life, I would really expect more than one roure per stop though.

## Architecture
- as simple as I could make it  
- value types for all models  
- structs everywhere to partition the namespace and provide separation of concern, ie: isolate the things that change from the ones that do not  
- a `WellKnown` struct that lists the "components" that app provides for reference by other such "components"
- layout by hand (my preferred, and by far, way of getting pixel perfect no matter what the actual screen size turns out to be)
- a unique view controller (because Apple requires one root view controller)
- judicious, purposeful and explicit use of GCD to always keep the UI responsive
- use of `SerialQueue1` for everything IO related, and `SerialQueue2` for everything computationally intensive.  

See my blog posts about [WellKnown](http://tinyurl.com/zylaeh2) and also about the [Single View Controller](http://tinyurl.com/jgyeknm)   

## Unit Testing  
- for _CaptainConf_, that which turns  

        if let url = WellKnown.Network.captainConf.captainURL([  
            "version":  Command.Version2_1.version  
        ,   "cmd":      Command.Version2_1.getbusnextdepartures]  
        ,   params:     [  
                ["mode":"line"]  
            ,   ["route":"0008"]  
            ,   ["direction":"1"]]) {  
           ...      
        }    
      

into   
    
      http://data.keolis-rennes.com/json/?key=E7Y2BTC5HJDNX17&cmd=getbusnextdepartures&version=2.1&param%5Bmode%5D=line&param%5Broute%5D=0008&param%5Bdirection%5D=1  

- for JSONParsing  


[Project Files](https://bitbucket.org/verec/captraincoding/src/a2a796a884e7d4d27e41c1cdd7fa22f5f9ad2fdb/project.png?at=master)  





