//
//  JSONParsingTests.swift
//  CapTrainCoding
//
//  Created by verec on 15/07/2016.
//  Copyright © 2016 Cantabilabs Ltd. All rights reserved.
//

import XCTest

@testable import CapTrainCoding

class JSONParsingTests: XCTestCase {
    
    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }

    func loadTestData(path: String) -> NSData? {
        if let rawJSONPath = NSBundle(forClass: JSONParsingTests.self).pathForResource(path, ofType: "json", inDirectory: "embedded") {
            return NSData(contentsOfFile:rawJSONPath)
        }
        return .None
    }

    func testGetBusNextDepartures() {
        /// 2016-07-15T18:18:55+02:00
        let departures = Model.Departure(departure: "2016-07-15T18:18:55+02:00")
        let expected = Model.Departures(departures:[departures])
        if let data = loadTestData("getbusnextdepartures") {
            if let jsonDict = WellKnown.Network.captainLoader.json(data) {
                if let departures: Model.Departures = WellKnown.Network.jsonParser.decode(jsonDict) {
                    XCTAssert(expected == departures, "data for getbusnextdepartures didn't match")
                } else {
                    XCTAssert(false, "data from getbusnextdepartures couldn't decoded")
                }
            } else {
                XCTAssert(false, "data from getbusnextdepartures couldn't be parsed")
            }
        } else {
            XCTAssert(false, "data from getbusnextdepartures couldn't be loaded")
        }
    }
}
