//
//  CaptainConfTests.swift
//  CapTrainCoding
//
//  Created by verec on 15/07/2016.
//  Copyright © 2016 Cantabilabs Ltd. All rights reserved.
//

import XCTest

@testable import CapTrainCoding

class CaptainConfTests: XCTestCase {
    
    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
    
    func testBasic() {

        let expected = "http://data.keolis-rennes.com/json/?key=E7Y2BTC5HJDNX17"

        if let url = WellKnown.Network.captainConf.captainURL() {
            let path = url.absoluteString
            XCTAssert(path == expected, "Wrong URL result")
        } else {
            XCTAssert(false, "url couldn't be creacted")
        }
    }

    func testGetStationVersion1() {

        let expected = "http://data.keolis-rennes.com/json/?key=E7Y2BTC5HJDNX17&cmd=getstation&version=1.0"

        if let url = WellKnown.Network.captainConf.captainURL([
            "version":  Command.Version1_0.version
        ,   "cmd":      Command.Version1_0.getstation]) {

            let path = url.absoluteString
            XCTAssert(path == expected, "Wrong URL result")
        } else {
            XCTAssert(false, "url couldn't be creacted")
        }
    }
    
    func testGetBusNextDeparturesVersion2_1() {

        let expected = "http://data.keolis-rennes.com/json/?key=E7Y2BTC5HJDNX17&cmd=getbusnextdepartures&version=2.1&param%5Bmode%5D=line&param%5Broute%5D=0008&param%5Bdirection%5D=1"

        if let url = WellKnown.Network.captainConf.captainURL([
            "version":  Command.Version2_1.version
        ,   "cmd":      Command.Version2_1.getbusnextdepartures]
        ,   params:     [
                ["mode":"line"]
            ,   ["route":"0008"]
            ,   ["direction":"1"]]) {

            let path = url.absoluteString
            XCTAssert(path == expected, "Wrong URL result")
        } else {
            XCTAssert(false, "url couldn't be creacted")
        }
    }
    

}
